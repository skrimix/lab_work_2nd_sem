﻿#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <iomanip>
#include <windows.h>
#include <vector>

using namespace std;

class Employees
{
public:
    struct employee
    {
        string lastname;
        string initials;
        string phonenumber;
    };

    int getcount()
    {
        return count;
    }

    void loadfile(string filename)
    {
        ifstream fin;
        fin.open(filename);
        string line;
        employee temp;
        list.clear();
        getline(fin, line);
        while (getline(fin, line))
        {
            istringstream sline(line);
            getline(sline, temp.lastname, ',');
            getline(sline, temp.initials, ',');
            getline(sline, temp.phonenumber, ',');
            list.push_back(temp);
        }
        fin.close();
        updatecount();
    }

    void addentry(string line)
    {
        employee temp;
        istringstream sline(line);
        getline(sline, temp.lastname, ' ');
        getline(sline, temp.initials, ' ');
        getline(sline, temp.phonenumber);
        list.push_back(temp);
        updatecount();
    }

    vector<employee>& getlist()
    {
        return list;
    }

    void removeentry(int k)
    {
        list.erase(list.begin() + (k - 1));
        updatecount();
    }

    void printout()
    {
        cout << "--------------------------------------------" << endl;
        cout << "| № |         ФИО         | Номер телефона |" << endl;
        cout << "--------------------------------------------" << endl;
        for (int i = 0; i < count; i++)
        {
            string name = string(list[i].lastname).append(" ").append(list[i].initials);
            cout << "|" << setw(3) << i + 1 << "|" << setw(21) << name << "|" << setw(16) << list[i].phonenumber << "|" << endl;
        };
    }

    void printout(vector<int>& c)
    {
        cout << "--------------------------------------------" << endl;
        cout << "| № |         ФИО         | Номер телефона |" << endl;
        cout << "--------------------------------------------" << endl;
        for (int k : c)
        {
            string name = string(list[k].lastname).append(" ").append(list[k].initials);
            cout << "|" << setw(3) << k + 1 << "|" << setw(21) << name << "|" << setw(16) << list[k].phonenumber << "|" << endl;
        }
    }

    void searchemployee(string s, vector<int>& c)
    {
        for (int i = 0; i < count; i++)
        {
            if (list[i].phonenumber == s)
            {
                c.push_back(i);
            }
        }
    }

private:
    int count = 0;
    vector<employee> list;

    void updatecount()
    {
        count = static_cast<int>(list.size());
    }
};

class Phone_numbers
{
public:
    struct phone_number
    {
        string number;
        int count = 0;
    };

    int getcount()
    {
        return count;
    }

    void createlist(vector<Employees::employee>& a)
    {
        bool fl;
        int k = 0, l = static_cast<int>(a.size());
        phone_number temp;
        list.clear();
        
        for (int i = 0; i < l; i++)
        {
            fl = false;
            for (int j = 0; j < k; j++)
                if (a[i].phonenumber == list[j].number)
                {
                    fl = true;
                    list[j].count++;
                }
            if (!fl)
            {
                temp.number = a[i].phonenumber;
                temp.count = 1;
                list.push_back(temp);
                k++;
            }
        }
        updatecount();
    }

    void printout()
    {
        cout << "-------------------------------------------" << endl;
        cout << "| № | Номер телефона | Кол-во сотрудников |" << endl;
        cout << "-------------------------------------------" << endl;
        for (int i = 0; i < count; i++)
            cout << "|" << setw(3) << i + 1 << "|" << setw(16) << list[i].number << "|" << setw(20) << list[i].count << endl;
    }

    void sortlistbynumber()
    {
        bool is_sorted = false;
        phone_number temp;
        while (!is_sorted)
        {
            is_sorted = true;
            for (int i = 0; i < count - 1; i++)
                if (strcmp(list[i].number.c_str(), list[i + 1].number.c_str()) > 0)
                {
                    temp = list[i];
                    list[i] = list[i + 1];
                    list[i + 1] = temp;
                    is_sorted = false;
                }
        }
    }

    void sortlistbycount()
    {
        bool is_sorted = false;
        phone_number temp;
        while (!is_sorted)
        {
            is_sorted = true;
            for (int i = 0; i < count - 1; i++)
                if (list[i].count < list[i + 1].count)
                {
                    temp = list[i];
                    list[i] = list[i + 1];
                    list[i + 1] = temp;
                    is_sorted = false;
                }
        }
    }

    void savetofile(string filename)
    {
        ofstream fout;
        fout.open(filename);
        fout << "phone_number,count\n";
        for (int i = 0; i < count; i++)
            fout << list[i].number << "," << list[i].count << "\n";
        fout.close();
    }

private:
    int count = 0;
    vector<phone_number> list;

    void updatecount()
    {
        count = static_cast<int>(list.size());
    }

};


int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    string selection;
    Employees employees;
    Phone_numbers phone_numbers;
    while (true)
    {
        cout << "\033[2J\033[1;1H";
        cout << "1.  Ввод базы данных из файла." << endl;
        cout << "2.  Вывод исходного вектора на экран." << endl;
        cout << "3.  Добавление записи в вектор." << endl;
        cout << "4.  Удаление записи из вектора." << endl;
        cout << "5.  Поиск сотрудника по номеру телефона." << endl;
        cout << "6.  Формирование перечня." << endl;
        cout << "7.  Вывод перечня на экран." << endl;
        cout << "8.  Сортировка перечня по номеру телефона." << endl;
        cout << "9.  Сортировка перечня по количеству сотрудников." << endl;
        cout << "10. Сохранение перечня в файл." << endl;
        cout << "11. Выход из программы" << endl;

        cout << "\nВаш выбор: ";
        getline(cin, selection);
        if (!selection.empty())
        {
            switch (stoi(selection))
            {
            case 1:
            {
                string filename, choice;
                cout << "Введите имя файла [employees.csv]:" << endl;
                getline(cin, filename);
                if (filename.empty()) filename = "employees.csv";
                employees.loadfile(filename);
                cout << "Файл загружен. Вывести введенные данные на экран? [д/Н]" << endl;
                getline(cin, choice);
                if (choice[0] == 'д' || choice[0] == 'Д')
                {
                    cout << "\033[2J\033[1;1H";
                    employees.printout();
                }
                break;
            }
            case 2:
            {
                cout << "\033[2J\033[1;1H";
                employees.printout();
                break;
            }
            case 3:
            {
                string line;
                cout << "Введите запись в формате: Фамилия И.О. Номер_телефона" << endl;
                getline(cin, line);
                employees.addentry(line);
                cout << "Запись добавлена." << endl;
                break;
            }
            case 4:
            {
                string line;
                int k;
                cout << "Введите номер записи для удаления:" << endl;
                getline(cin, line);
                k = stoi(line);
                if (k <= employees.getcount())
                {
                    employees.removeentry(k);
                    cout << "Запись удалена." << endl;
                }
                else
                    cout << "Записи под данным номером не существует." << endl;
                break;
            }
            case 5:
            {
                string num;
                cout << "Введите номер телефона:" << endl;
                getline(cin, num);
                vector<int> c;
                employees.searchemployee(num, c);
                int k = static_cast<int>(c.size());
                cout << "\033[2J\033[1;1H";
                if (k < 1)
                    cout << "Сотрудников с данным номером не найдено.";
                else
                {
                    cout << "Найдено сотрудников: " << k << endl;
                    employees.printout(c);
                }
                break;
            }
            case 6:
            {
                string choice;
                phone_numbers.createlist(employees.getlist());
                cout << "Перечень сформирован. Вывести сформированный перечень на экран? [д/Н]" << endl;
                getline(cin, choice);
                if (choice[0] == 'д' || choice[0] == 'Д')
                {
                    cout << "\033[2J\033[1;1H";
                    phone_numbers.printout();
                }
                break;
            }
            case 7:
            {
                cout << "\033[2J\033[1;1H";
                phone_numbers.printout();
                break;
            }
            case 8:
            {
                phone_numbers.sortlistbynumber();
                cout << "Перечень отсортирован по номеру телефона.";
                break;
            }
            case 9:
            {
                phone_numbers.sortlistbycount();
                cout << "Перечень отсортирован по количеству сотрудников.";
                break;
            }
            case 10:
            {
                string filename;
                cout << "Введите имя файла [phonenumbers.csv]:" << endl;
                getline(cin, filename);
                if (filename.empty()) filename = "phonenumbers.csv";
                phone_numbers.savetofile(filename);
                cout << "Перечень сохранен в файл.";
                break;
            }
            case 11:
                exit(0);
            default:
            {
                cout << "Неверный выбор.";
                break;
            }
            }
        }
        else
            cout << "Не введен выбор.";
        cout << "\nНажмите ENTER для возврата в меню...";
        cin.get();
    }

};